import { StatusBar as ExpoStatusBar } from 'expo-status-bar';
import React from 'react';
import { ThemeProvider } from 'styled-components/native';

import {
  useFonts as useOswald,
  Oswald_400Regular,
} from '@expo-google-fonts/oswald';
import { useFonts as useLato, Lato_400Regular } from '@expo-google-fonts/lato';

import { theme } from './src/infrastructure/theme';
import { Navigation } from './src/infrastructure/navigation';

import { firebaseConfig } from './src/utils/firebase.config';
import { getApps, initializeApp } from 'firebase/app';
import { AuthenticationContextProvider } from './src/services/authentication/authentication.context';
import {
  initializeAuth,
  getReactNativePersistence,
} from 'firebase/auth/react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

if (getApps().length < 1) {
  const firebaseApp = initializeApp(firebaseConfig);
  // required when using firebase v9 with AsyncStorage, otherwise warning -
  // "AsyncStorage will be removed from react-native core in the future release.
  // It should be installed and imported from '@react-native-async-storage/async-storage'
  // instead of 'react-native'."
  // even when react-native-async-storage/async-storage is already used
  initializeAuth(firebaseApp, {
    persistence: getReactNativePersistence(AsyncStorage),
  });
}

const App = () => {
  const [oswaldLoaded] = useOswald({
    Oswald_400Regular,
  });

  const [latoLoaded] = useLato({
    Lato_400Regular,
  });

  if (!oswaldLoaded || !latoLoaded) {
    return null;
  }

  return (
    <>
      <ThemeProvider theme={theme}>
        <AuthenticationContextProvider>
          <Navigation />
        </AuthenticationContextProvider>
      </ThemeProvider>
      <ExpoStatusBar style="auto" />
    </>
  );
};

export default App;
