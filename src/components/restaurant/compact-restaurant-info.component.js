import React from 'react';
import styled from 'styled-components/native';
import WebView from 'react-native-webview';
import { Platform, Image } from 'react-native';

import { Text } from '../typography/text.component';

const CompactImage = styled.Image`
  border-radius: 10px;
  width: 120px;
  height: 100px;
`;

const CompactWebview = styled(WebView)`
  border-radius: 10px;
  width: 120px;
  height: 100px;
`;

const Item = styled.View`
  padding: 10px;
  max-width: 120px;
  height: 100px;
  align-items: center;
`;

const isAndroid = Platform.OS === 'android';

export const CompactRestaurantInfo = ({ restaurant, isMap }) => {
  // does not work - video 048
  // const Image = isAndroid && isMap ? CompactWebview : CompactImage;
  const Image = isAndroid ? CompactWebview : CompactImage;

  return (
    <Item>
      <Image
        source={{ uri: restaurant.photos[0] }}
        style={{ height: '100%' }}
      />
      <Text center variant="caption" numberOfLines={3}>
        {restaurant.name}
      </Text>
    </Item>
  );
};
