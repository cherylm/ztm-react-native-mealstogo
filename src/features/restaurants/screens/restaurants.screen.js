import React, {useContext, useState} from 'react';
import styled from 'styled-components/native';
import {FlatList, TouchableOpacity} from 'react-native';
import {Searchbar, ActivityIndicator, Colors} from 'react-native-paper';
import {RestaurantInfoCard} from '../components/restaurant-info-card.component';
import {Spacer} from '../../../components/spacer/spacer.component';
import {SafeArea} from '../../../components/utility/safe-area.component';

import {RestaurantsContext} from '../../../services/restaurants/restaurants.context';
import {FavouritesContext} from '../../../services/favourites/favourites.context';

import {Search} from '../components/search.component';
import {FavouritesBar} from '../../../components/favourites/favourites-bar.component';
import {FadeInView} from '../../../components/animations/fade.animation';

const LoadingContainer = styled.View`
  position: absolute;
  top: 50%;
  left: 50%;
`;

const Loading = styled(ActivityIndicator)`
  margin-left: -30px;
`;

const RestaurantList = styled(FlatList).attrs({
    contentContainerStyle: {
        padding: 16,
    },
})``;

export const RestaurantsScreen = ({navigation}) => {
    const {isLoading, error, restaurants} = useContext(RestaurantsContext);
    const [isToggled, setIsToggled] = useState(false);

    const {favourites} = useContext(FavouritesContext);

    return (
        <SafeArea>
            {isLoading && (
                <LoadingContainer>
                    <Loading animating={true} color={Colors.blue300} size={60}/>
                </LoadingContainer>
            )}
            <Search
                isFavouritesToggled={isToggled}
                onFavouritesToggle={() => setIsToggled(!isToggled)}
            />
            {isToggled && (
                <FavouritesBar
                    favourites={favourites}
                    onNavigate={navigation.navigate}
                />
            )}

            <RestaurantList
                data={restaurants}
                renderItem={({item}) => {
                    //console.log(item);
                    return (
                        <TouchableOpacity
                            onPress={() =>
                                navigation.navigate('RestaurantDetail', {restaurant: item})
                            }
                        >
                            <Spacer position="bottom" size="large">
                                <FadeInView>
                                    <RestaurantInfoCard restaurant={item}/>
                                </FadeInView>
                            </Spacer>
                        </TouchableOpacity>
                    );
                }}
                keyExtractor={(item) => item.name}
                contentContainerStyle={{padding: 16}}
            />

        </SafeArea>
    );
};
