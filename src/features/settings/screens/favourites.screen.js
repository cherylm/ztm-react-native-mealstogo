import React, {useContext} from 'react'
import {SafeArea} from '../../../components/utility/safe-area.component';
import {FlatList, TouchableOpacity} from 'react-native';
import {Spacer} from '../../../components/spacer/spacer.component';
import {RestaurantInfoCard} from '../../restaurants/components/restaurant-info-card.component';
import {FavouritesContext} from '../../../services/favourites/favourites.context';
import styled from 'styled-components/native';
import {Text} from '../../../components/typography/text.component';

const NoFavouritesArea = styled(SafeArea)`
  align-items: center;
  justify-content: center;
`;

const RestaurantList = styled(FlatList).attrs({
    contentContainerStyle: {
        padding: 16,
    },
})``;

export const FavouritesScreen = ({navigation}) => {
    const { favourites } = useContext(FavouritesContext);
    return favourites.length? (<SafeArea>
        <RestaurantList
            data={favourites}
            renderItem={({ item }) => {
                return (
                    <TouchableOpacity
                        onPress={() =>
                            navigation.navigate('RestaurantDetail', { restaurant: item })
                        }
                    >
                        <Spacer position="bottom" size="large">
                            <RestaurantInfoCard restaurant={item} />
                        </Spacer>
                    </TouchableOpacity>
                );
            }}
            keyExtractor={(item) => item.name}
            contentContainerStyle={{ padding: 16 }}
        />
    </SafeArea>):(<NoFavouritesArea>
        <Text center>No favourites yet</Text>
    </NoFavouritesArea>)
}