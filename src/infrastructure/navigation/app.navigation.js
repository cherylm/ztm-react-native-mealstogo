import React, { useContext } from 'react';
import { Ionicons } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';


import { RestaurantsNavigator } from './restaurants.navigator';
import { MapScreen } from '../../features/map/screens/map.screen';
import { FavouritesContextProvider } from '../../services/favourites/favourites.context';
import { LocationContextProvider } from '../../services/location/location.context';
import { RestaurantsContextProvider } from '../../services/restaurants/restaurants.context';
import {SettingsScreen} from '../../features/settings/screens/settings.screen';
import {SettingsNavigator} from './settings.navigator';

const Tab = createBottomTabNavigator();

const TAB_ICON = {
  Restaurants: 'restaurant-outline',
  Map: 'map-outline',
  Settings: 'settings-outline',
};



const createScreenOptions = ({ route }) => {
  const iconName = TAB_ICON[route.name];
  return {
    tabBarIcon: ({ size, color }) => (
      <Ionicons name={iconName} size={size} color={color} />
    ),
    tabBarOptions: {
      activeTintColor: '#27BAFC',
      inactiveTintColor: 'gray',
    },
    headerShown: false,
  };
};

export const AppNavigator = () => (
  <FavouritesContextProvider>
    <LocationContextProvider>
      <RestaurantsContextProvider>
        <Tab.Navigator screenOptions={createScreenOptions}>
          <Tab.Screen
            name="Restaurants"
            component={RestaurantsNavigator}
          ></Tab.Screen>
          <Tab.Screen name="Map" component={MapScreen}></Tab.Screen>
          <Tab.Screen name="Settings" component={SettingsNavigator}></Tab.Screen>
        </Tab.Navigator>
      </RestaurantsContextProvider>
    </LocationContextProvider>
  </FavouritesContextProvider>
);
