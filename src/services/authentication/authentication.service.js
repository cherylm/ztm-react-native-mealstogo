// Note: getting rid of this file, old code for firebase v8 doesn't work anymore with firebase v9
// .then and catch block has to be in this file, easier to put it in authentication.context

/* code for v9
import { getAuth, signInWithEmailAndPassword } from 'firebase/auth';

export const loginRequest = (email, password) => {
  const auth = getAuth();
  signInWithEmailAndPassword(auth, email, password)
    .then((user) => console.log(user))
    .catch((e) => {
      console.log(e);
    });
};*/

/* code for v8
import * as firebase from 'firebase';

export const loginRequest = (email, password) =>
  firebase.auth().signInWithEmailAndPassword(email, password);
*/
